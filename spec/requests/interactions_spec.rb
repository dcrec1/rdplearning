require 'rails_helper'

describe 'API' do
  let(:user) { create :user }
  let(:json) { JSON.parse(response.body) }

  describe 'GET /users/interactions' do
    it 'renders the interactions' do
      create :interaction
      2.times { create :interaction, user: user }
      get '/users/interactions.json', params: { auth_token: user.authentication_token }
      expect(json.count).to eql(2)
    end
  end

  describe 'POST /users/activities/:activity_id/interaction' do
    let(:activity) { create :activity }

    it 'creates an interaction for the user' do
      post "/users/activities/#{activity.id}/interaction", params: { auth_token: user.authentication_token, interaction: { status: 'done', duration: 101 } }, as: :json
      interaction = user.interactions.find_by(activity_id: activity.id)
      expect(interaction.status).to eql('done')
      expect(interaction.duration).to eql(101)
    end

    it 'updates an interaction for the user' do
      interaction = create :interaction, user: user, activity: activity, status: 'to-do'
      post "/users/activities/#{activity.id}/interaction", params: { auth_token: user.authentication_token, interaction: { status: 'done' } }, as: :json
      interaction.reload
      expect(interaction.status).to eql('done')
    end
  end
end
