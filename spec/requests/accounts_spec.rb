require 'rails_helper'

describe 'API' do
  let(:user) { create :user }
  let(:json) { JSON.parse(response.body) }

  describe 'GET /users/account' do
    it 'renders the current user' do
      get '/users/account.json', params: { auth_token: user.authentication_token }
      expect(json['email']).to eql(user.email)
    end
  end

  describe 'GET /users/performance' do
    it 'renders the current user performance' do
      second_activity = create :activity
      first_activity = create :activity
      create :activities_context, activity: second_activity, position: 2
      create :activities_context, activity: first_activity, position: 1
      second_answer = create :answer, activity: second_activity, given_option: 1, expected_option: 2, user: user
      first_answer = create :answer, activity: first_activity, given_option: 1, expected_option: 1, user: user
      2.times { create :answer, activity: second_activity, given_option: 1, expected_option: 1, object_id: second_answer.object_id }
      2.times { create :answer, activity: first_activity, given_option: 1, expected_option: 2, object_id: first_answer.object_id }
      get '/users/performance.json', headers: { "HTTP_AUTHORIZATION": encode_credentials(user.authentication_token) }
      expect(json[0]['score']).to eql(1)
      expect(json[0]['average'].to_i).to eql(0)
      expect(json[1]['score']).to eql(0)
      expect(json[1]['average'].to_i).to eql(1)
    end
  end
end
