require 'rails_helper'

describe 'API' do
  let(:user) { create :user }
  let(:json) { JSON.parse(response.body) }

  describe 'GET /users/contexts' do
    it 'renders the current user contexts' do
      parent_context = create :context
      child_context = create :context, parent: parent_context
      create :context, parent: parent_context
      grand_child_context = create :context, parent: child_context
      activities_context = Array.new(2) { |i| create :activities_context, context: grand_child_context, position: (2 - i) }
      create :membership, context: child_context, user: user
      get '/users/contexts.json', params: { auth_token: user.authentication_token }
      expect(json[0]['id']).to eql(parent_context.id)
      expect(json[0]['children'][0]['id']).to eql(child_context.id)
      expect(json[0]['children'][0]['children'][0]['id']).to eql(grand_child_context.id)
      expect(json[0]['children'][0]['children'][0]['activities'].count).to eql(2)
      expect(json[0]['children'][0]['children'][0]['activities'][0]['id']).to eql(activities_context.last.activity.id)
    end
  end
end
