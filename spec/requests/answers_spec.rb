require 'rails_helper'

describe 'API' do
  let(:activity) { create :activity }
  let(:user) { create :user }
  let(:json) { JSON.parse(response.body) }

  describe 'GET /users/activities/:activity_id/answers' do
    it 'renders the ansers' do
      create :answer, activity: activity
      2.times { create :answer, user: user, activity: activity }
      create :answer, user: user
      get "/users/activities/#{activity.id}/answers.json", headers: { "HTTP_AUTHORIZATION": encode_credentials(user.authentication_token) }
      expect(json.count).to eql(2)
    end
  end

  describe 'POST /users/activities/:activity_id/answers' do
    it 'creates an answer for the user' do
      post "/users/activities/#{activity.id}/answers", params: { auth_token: user.authentication_token, answer: { object_id: 'OBJECT_ID', expected_option: '1a', given_option: '2b', kind: 'rt' }}, as: :json
      answer = user.answers.find_by(activity_id: activity.id)
      expect(answer.object_id).to eql('OBJECT_ID')
      expect(answer.expected_option).to eql('1a')
      expect(answer.given_option).to eql('2b')
      expect(answer.kind).to eql('rt')
    end
  end
end
