require 'rails_helper'

describe 'API' do
  let(:user) { create :user }
  let(:json) { JSON.parse(response.body) }

  describe 'GET /users/events' do
    it 'renders the current user context events' do
      parent_context = create :context
      child_context = create :context, parent: parent_context
      other_child_context = create :context, parent: parent_context
      grand_child_context = create :context, parent: child_context
      create :membership, context: child_context, user: user
      parent_event = create :event, context: parent_context
      grand_child_event = create :event, context: grand_child_context
      create :event, context: other_child_context
      get '/users/events.json', params: { auth_token: user.authentication_token }
      expect(json.map { |element| element['id'] }.sort).to match_array([parent_event.id, grand_child_event.id])
    end
  end
end
