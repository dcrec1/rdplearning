require 'rails_helper'

describe 'API' do
  let(:user) { create :user }
  let(:json) { JSON.parse(response.body) }

  describe 'GET /users/notification' do
    it 'renders the current user context notifications' do
      parent_context = create :context
      child_context = create :context, parent: parent_context
      other_child_context = create :context, parent: parent_context
      grand_child_context = create :context, parent: child_context
      create :membership, context: child_context, user: user
      parent_notification = create :notification, context: parent_context
      grand_child_notification = create :notification, context: grand_child_context
      create :notification, context: other_child_context
      user_notification = create :notification, user: user
      get '/users/notifications.json', params: { auth_token: user.authentication_token }
      expect(json.map { |element| element['id'] }.sort).to match_array([parent_notification.id, grand_child_notification.id, user_notification.id])
    end
  end
end
