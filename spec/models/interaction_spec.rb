require 'rails_helper'

RSpec.describe Interaction, type: :model do
  context 'on creation' do
    let(:context) { create :context }
    let(:user) { create :user }

    it 'creates a notification if has interactions for all activities' do
      create :activity
      2.times do
        activity = create :activity
        create :activities_context, context: context, activity: activity
        create :interaction, activity: activity, user: user
      end
      expect(Notification.where(user_id: user)).to_not be_empty
    end

    it "doesnt create a notification if does'tn has interactions for all activities" do
      other_activity = create :activity
      create :activities_context, context: context, activity: other_activity
      2.times do
        activity = create :activity
        create :activities_context, context: context, activity: activity
        create :interaction, activity: activity, user: user
      end
      expect(Notification.where(user_id: user)).to be_empty
    end
  end
end
