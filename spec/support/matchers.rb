def validates_presence_of(*attributes)
  attributes.each do |attribute|
    it "validates presence of #{attribute}" do
      expect(build(subject.class.to_s.underscore, attribute => nil)).to_not be_valid
    end
  end
end

def validates_uniquenes_of(*attributes)
  attributes.each do |attribute|
    it "validates uniqueness of #{attribute}" do
      record = create subject.class.to_s.underscore
      expect(build(subject.class.to_s.underscore, attribute => record.send(attribute))).to_not be_valid
    end
  end
end
