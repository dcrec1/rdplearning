FactoryGirl.define do
  factory :answer do
    association :user
    sequence(:object_id) { |n| "q#{n}" }
  end

  factory :activity do
  end

  factory :activities_context do
    association :activity
    association :context
  end

  factory :context do
    association :level
  end

  factory :event do
  end

  factory :level do
    parent { Level.last }
  end

  factory :notification do
  end

  factory :interaction do
  end

  factory :membership do
  end

  factory :organization do
    sequence(:uid) { |n| "org#{n}" }
  end

  factory :user do
    sequence(:email) { |n| "user#{n}@email.com" }
    password 'password'
  end
end
