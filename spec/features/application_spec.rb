require 'rails_helper'

describe 'Application' do
  let(:organization) { create :organization, uid: 'anima' }
  let(:user) { create :user, organization: organization }

  context '', js: true do
    it 'signs in via oauth' do
      Capybara.default_host = "http://#{organization.uid}.rdplearning.test"
      Capybara.server_port = 9887
      Capybara.app_host = "#{Capybara.default_host}:#{Capybara.server_port}"

      OmniAuth.config.add_mock :office365, uid: 'UID', info: { email: user.email }
      visit '/users/auth/office365?redirect_url=http://pudim.com.br/'
      expect(current_url).to match(/http:\/\/pudim\.com\.br\/\?auth_token=(.+)/)

      visit '/users/auth/office365?redirect_url=http://pudim.com.br?a=b'
      expect(current_url).to match(/http:\/\/pudim\.com\.br\/\?a=b&auth_token=(.+)/)

      OmniAuth.config.add_mock :office365, uid: 'UID', info: { email: 'unknow@email.com' }
      visit '/users/auth/office365?redirect_url=http://pudim.com.br'
      expect(current_url).to eql('http://pudim.com.br/?auth_token=')

      other_user = create :user
      OmniAuth.config.add_mock :office365, uid: 'UID', info: { email: other_user.email }
      visit '/users/auth/office365?redirect_url=http://www.google.com.br'
      expect(current_url).to eql('http://www.google.com.br/?auth_token=')
    end
  end
end
