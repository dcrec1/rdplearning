class AddOrganizationIdToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :organization_id, :integer
    add_index :projects, :organization_id
  end
end
