class AddOrganizationIdToContexts < ActiveRecord::Migration[5.0]
  def change
    add_column :contexts, :organization_id, :integer
    add_index :contexts, :organization_id
  end
end
