class RemoveContextFromActivities < ActiveRecord::Migration[5.0]
  def change
    remove_column :activities, :context_id
  end
end
