class AddAncestryDepthToContexts < ActiveRecord::Migration[5.0]
  def change
    add_column :contexts, :ancestry_depth, :integer, null: false, default: 0
  end
end
