class AddRdpLibraryTokenToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :rdp_library_token, :string
  end
end
