class CreateActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :activities do |t|
      t.references :context
      t.string :name
      t.timestamps
    end
  end
end
