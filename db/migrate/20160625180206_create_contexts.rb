class CreateContexts < ActiveRecord::Migration[5.0]
  def change
    create_table :contexts do |t|
      t.string :name
      t.string :ancestry
      t.references :system

      t.timestamps
    end
    add_index :contexts, :ancestry
  end
end
