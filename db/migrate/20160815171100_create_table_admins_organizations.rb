class CreateTableAdminsOrganizations < ActiveRecord::Migration[5.0]
  def change
    create_table :admins_organizations, id: false do |t|
      t.references :admin, foreign_key: true, index: true
      t.references :organization, foreign_key: true, index: true
    end
  end
end
