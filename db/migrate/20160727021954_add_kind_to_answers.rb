class AddKindToAnswers < ActiveRecord::Migration[5.0]
  def change
    add_column :answers, :kind, :string
    change_column :answers, :given_option, :string
    change_column :answers, :expected_option, :string
  end
end
