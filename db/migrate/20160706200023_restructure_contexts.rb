class RestructureContexts < ActiveRecord::Migration[5.0]
  def change
    rename_column :contexts, :project_id, :level_id
  end
end
