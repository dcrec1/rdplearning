class AddAttributesToActivities < ActiveRecord::Migration[5.0]
  def change
    add_column :activities, :duration, :integer
    add_column :activities, :properties, :text
  end
end
