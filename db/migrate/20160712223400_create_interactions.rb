class CreateInteractions < ActiveRecord::Migration[5.0]
  def change
    create_table :interactions do |t|
      t.references :user, foreign_key: true
      t.references :activity, foreign_key: true
      t.string :status

      t.timestamps
    end
  end
end
