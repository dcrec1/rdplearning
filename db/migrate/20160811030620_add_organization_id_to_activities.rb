class AddOrganizationIdToActivities < ActiveRecord::Migration[5.0]
  def change
    add_column :activities, :organization_id, :integer
    add_index :activities, :organization_id
  end
end
