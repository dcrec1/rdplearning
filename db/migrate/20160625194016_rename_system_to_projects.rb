class RenameSystemToProjects < ActiveRecord::Migration[5.0]
  def change
    rename_table :systems, :projects
    rename_column :contexts, :system_id, :project_id
  end
end
