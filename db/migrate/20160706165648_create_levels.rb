class CreateLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :levels do |t|
      t.string :name
      t.integer :parent_id

      t.timestamps
    end
    add_index :levels, :parent_id, unique: true
  end
end
