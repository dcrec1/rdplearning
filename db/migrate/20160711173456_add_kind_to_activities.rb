class AddKindToActivities < ActiveRecord::Migration[5.0]
  def change
    add_column :activities, :kind, :integer
  end
end
