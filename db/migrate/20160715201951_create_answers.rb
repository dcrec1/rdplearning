class CreateAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :answers do |t|
      t.references :activity, foreign_key: true
      t.string :object_id
      t.references :user, foreign_key: true
      t.integer :given_option
      t.integer :expected_option

      t.timestamps
    end
  end
end
