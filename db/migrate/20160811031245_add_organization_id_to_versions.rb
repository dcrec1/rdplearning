class AddOrganizationIdToVersions < ActiveRecord::Migration[5.0]
  def change
    add_column :versions, :organization_id, :integer
    add_index :versions, :organization_id
  end
end
