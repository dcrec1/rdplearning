class AddKeysToOrganization < ActiveRecord::Migration[5.0]
  def change
    add_column :organizations, :office365_app_id, :string
    add_column :organizations, :office365_app_secret, :string
  end
end
