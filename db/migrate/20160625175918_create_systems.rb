class CreateSystems < ActiveRecord::Migration[5.0]
  def change
    create_table :systems do |t|
      t.string :name
      t.string :uid

      t.timestamps
    end
    add_index :systems, :uid
  end
end
