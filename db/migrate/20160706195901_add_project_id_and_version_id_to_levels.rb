class AddProjectIdAndVersionIdToLevels < ActiveRecord::Migration[5.0]
  def change
    add_column :levels, :project_id, :integer
    add_index :levels, :project_id
    add_column :levels, :version_id, :integer
    add_index :levels, :version_id
  end
end
