class CreateAddActivitiesContexts < ActiveRecord::Migration[5.0]
  def change
    create_table :activities_contexts do |t|
      t.references :activity, index: true
      t.references :context, index: true
      t.integer :position
    end
  end
end
