class AddPositionToContexts < ActiveRecord::Migration[5.0]
  def change
    add_column :contexts, :position, :integer
  end
end
