class AddDurationToInteraction < ActiveRecord::Migration[5.0]
  def change
    add_column :interactions, :duration, :integer
  end
end
