# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160816221627) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "duration"
    t.text     "properties"
    t.integer  "kind"
    t.integer  "organization_id"
    t.index ["organization_id"], name: "index_activities_on_organization_id", using: :btree
  end

  create_table "activities_contexts", force: :cascade do |t|
    t.integer "activity_id"
    t.integer "context_id"
    t.integer "position"
    t.index ["activity_id"], name: "index_activities_contexts_on_activity_id", using: :btree
    t.index ["context_id"], name: "index_activities_contexts_on_context_id", using: :btree
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "admins_organizations", id: false, force: :cascade do |t|
    t.integer "admin_id"
    t.integer "organization_id"
    t.index ["admin_id"], name: "index_admins_organizations_on_admin_id", using: :btree
    t.index ["organization_id"], name: "index_admins_organizations_on_organization_id", using: :btree
  end

  create_table "answers", force: :cascade do |t|
    t.integer  "activity_id"
    t.string   "object_id"
    t.integer  "user_id"
    t.string   "given_option"
    t.string   "expected_option"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "kind"
    t.index ["activity_id"], name: "index_answers_on_activity_id", using: :btree
    t.index ["user_id"], name: "index_answers_on_user_id", using: :btree
  end

  create_table "contexts", force: :cascade do |t|
    t.string   "name"
    t.string   "ancestry"
    t.integer  "level_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "position"
    t.integer  "ancestry_depth",  default: 0, null: false
    t.integer  "organization_id"
    t.index ["ancestry"], name: "index_contexts_on_ancestry", using: :btree
    t.index ["level_id"], name: "index_contexts_on_level_id", using: :btree
    t.index ["organization_id"], name: "index_contexts_on_organization_id", using: :btree
  end

  create_table "events", force: :cascade do |t|
    t.integer  "context_id"
    t.string   "title"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["context_id"], name: "index_events_on_context_id", using: :btree
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "interactions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "activity_id"
    t.string   "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "duration"
    t.index ["activity_id"], name: "index_interactions_on_activity_id", using: :btree
    t.index ["user_id"], name: "index_interactions_on_user_id", using: :btree
  end

  create_table "levels", force: :cascade do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "project_id"
    t.integer  "version_id"
    t.index ["parent_id"], name: "index_levels_on_parent_id", unique: true, using: :btree
    t.index ["project_id"], name: "index_levels_on_project_id", using: :btree
    t.index ["version_id"], name: "index_levels_on_version_id", using: :btree
  end

  create_table "memberships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "context_id"
    t.integer  "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["context_id"], name: "index_memberships_on_context_id", using: :btree
    t.index ["user_id"], name: "index_memberships_on_user_id", using: :btree
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "context_id"
    t.string   "title"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["context_id"], name: "index_notifications_on_context_id", using: :btree
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "organizations", force: :cascade do |t|
    t.string   "name"
    t.string   "uid"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "office365_app_id"
    t.string   "office365_app_secret"
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.string   "uid"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "organization_id"
    t.index ["organization_id"], name: "index_projects_on_organization_id", using: :btree
    t.index ["uid"], name: "index_projects_on_uid", using: :btree
  end

  create_table "rpush_apps", force: :cascade do |t|
    t.string   "name",                                null: false
    t.string   "environment"
    t.text     "certificate"
    t.string   "password"
    t.integer  "connections",             default: 1, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "type",                                null: false
    t.string   "auth_key"
    t.string   "client_id"
    t.string   "client_secret"
    t.string   "access_token"
    t.datetime "access_token_expiration"
  end

  create_table "rpush_feedback", force: :cascade do |t|
    t.string   "device_token", limit: 64, null: false
    t.datetime "failed_at",               null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "app_id"
    t.index ["device_token"], name: "index_rpush_feedback_on_device_token", using: :btree
  end

  create_table "rpush_notifications", force: :cascade do |t|
    t.integer  "badge"
    t.string   "device_token",      limit: 64
    t.string   "sound",                        default: "default"
    t.text     "alert"
    t.text     "data"
    t.integer  "expiry",                       default: 86400
    t.boolean  "delivered",                    default: false,     null: false
    t.datetime "delivered_at"
    t.boolean  "failed",                       default: false,     null: false
    t.datetime "failed_at"
    t.integer  "error_code"
    t.text     "error_description"
    t.datetime "deliver_after"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.boolean  "alert_is_json",                default: false
    t.string   "type",                                             null: false
    t.string   "collapse_key"
    t.boolean  "delay_while_idle",             default: false,     null: false
    t.text     "registration_ids"
    t.integer  "app_id",                                           null: false
    t.integer  "retries",                      default: 0
    t.string   "uri"
    t.datetime "fail_after"
    t.boolean  "processing",                   default: false,     null: false
    t.integer  "priority"
    t.text     "url_args"
    t.string   "category"
    t.boolean  "content_available",            default: false
    t.text     "notification"
    t.index ["delivered", "failed"], name: "index_rpush_notifications_multi", where: "((NOT delivered) AND (NOT failed))", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "authentication_token"
    t.string   "uid"
    t.string   "name"
    t.string   "avatar"
    t.string   "phone"
    t.string   "login"
    t.string   "rdp_library_token"
    t.integer  "organization_id"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["organization_id"], name: "index_users_on_organization_id", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "versions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "organization_id"
    t.index ["organization_id"], name: "index_versions_on_organization_id", using: :btree
  end

  add_foreign_key "admins_organizations", "admins"
  add_foreign_key "admins_organizations", "organizations"
  add_foreign_key "answers", "activities"
  add_foreign_key "answers", "users"
  add_foreign_key "interactions", "activities"
  add_foreign_key "interactions", "users"
  add_foreign_key "memberships", "contexts"
  add_foreign_key "memberships", "users"
  add_foreign_key "notifications", "contexts"
end
