Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/_admin', as: 'rails_admin'
  devise_for :admins
  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
  devise_scope :user do
    match '/users/auth/:provider/setup' => 'users/omniauth_callbacks#setup', via: :get
  end
  namespace :admins do
    %i(activities activities_contexts answers contexts events interactions memberships notifications levels projects users versions).each do |route|
      resources route.to_sym
    end
    root to: 'levels#index'
  end
  namespace :users do
    resource :account
    get 'performance' => 'accounts#performance'
    namespace :publisher do
      get :token
    end
    resources :contexts
    resources :events
    resources :notifications
    resources :interactions
    resources :activities do
      resources :answers
      resource :interaction
    end
  end
end
