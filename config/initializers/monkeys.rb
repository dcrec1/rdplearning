require 'omniauth-oauth2'

OmniAuth::Strategies::Office365.class_eval do
  option :authorize_params, {
    resource: 'https://graph.windows.net/',
    domain_hint: 'office.digitalpages.com.br'
  }
end
