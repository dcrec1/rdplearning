module Admins
  class VersionsController < ResourcefulController
    def permitted_attributes
      [:name]
    end
  end
end
