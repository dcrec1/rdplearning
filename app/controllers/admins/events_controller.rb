module Admins
  class EventsController < ResourcefulController
    def permitted_attributes
      [:context_id, :title, :starts_at, :ends_at]
    end
  end
end
