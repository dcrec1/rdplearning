module Admins
  class MembershipsController < ResourcefulController
    def permitted_attributes
      [:user_id, :context_id, :role]
    end
  end
end
