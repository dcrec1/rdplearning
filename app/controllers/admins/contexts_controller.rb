module Admins
  class ContextsController < ResourcefulController
    def permitted_attributes
      [:name, :level_id, :ancestry, :position]
    end
  end
end
