module Admins
  class UsersController < ResourcefulController
    def permitted_attributes
      [:name, :email, :login, :avatar, :phone, :rdp_library_token]
    end
  end
end
