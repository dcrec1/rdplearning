module Admins
  class LevelsController < ResourcefulController
    def permitted_attributes
      [:name, :parent_id, :project_id, :version_id]
    end
  end
end
