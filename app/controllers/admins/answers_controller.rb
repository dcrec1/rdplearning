module Admins
  class AnswersController < ResourcefulController
    def permitted_attributes
      [:user_id, :activity_id, :object_id, :given_option, :expected_option, :kind]
    end
  end
end
