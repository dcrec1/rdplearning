module Admins
  class ActivitiesContextsController < ResourcefulController
    def permitted_attributes
      [:activity_id, :context_id, :position]
    end
  end
end
