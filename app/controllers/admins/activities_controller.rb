module Admins
  class ActivitiesController < ResourcefulController
    def permitted_attributes
      [:name, :duration, :properties, :kind]
    end
  end
end
