module Admins
  class NotificationsController < ResourcefulController
    def permitted_attributes
      [:title, :body, :context_id, :user_id]
    end
  end
end
