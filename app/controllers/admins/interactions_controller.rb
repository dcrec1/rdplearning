module Admins
  class InteractionsController < ResourcefulController
    def permitted_attributes
      [:user_id, :activity_id, :status, :duration]
    end
  end
end
