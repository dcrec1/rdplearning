module Admins
  class ProjectsController < ResourcefulController
    def permitted_attributes
      [:name, :uid]
    end
  end
end
