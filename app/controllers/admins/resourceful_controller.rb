module Admins
  class ResourcefulController < ApplicationController
    inherit_resources
    has_scope :page, default: 1
    has_scope :order, default: 'id desc'
    before_filter :authenticate_admin!
    helper_method :current_organization, :permitted_attributes
    layout 'admin'

    private

    def begin_of_association_chain
      current_organization
    end

    def permitted_params
      params.permit(controller_name.singularize => permitted_attributes)
    end
  end
end
