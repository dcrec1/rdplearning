class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  respond_to :html, :json

  def current_organization
    @organization ||= Organization.find_by!(uid: request.host.split('.')[0])
  end
end
