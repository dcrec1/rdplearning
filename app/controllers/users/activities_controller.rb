module Users
  class ActivitiesController < BaseController
    def show
      respond_with Activity.find(params[:id])
    end
  end
end
