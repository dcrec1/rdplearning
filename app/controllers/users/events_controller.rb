module Users
  class EventsController < BaseController
    def index
      respond_with current_user.events
    end
  end
end
