module Users
  class ContextsController < BaseController
    def index
      respond_with current_user.contexts.map(&:tree)
    end
  end
end
