module Users
  class Users::AnswersController < BaseController
    def index
      respond_with collection.where(activity_id: params[:activity_id])
    end

    def create
      resource = collection.create!(permitted_params.merge(activity: parent))
      respond_with :users, parent, resource
    end

    private

    def parent
      @activity ||= Activity.find(params[:activity_id])
    end

    def collection
      current_user.answers
    end

    def permitted_params
      params.require(:answer).permit(:object_id, :expected_option, :given_option, :kind)
    end
  end
end
