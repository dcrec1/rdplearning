module Users
  class Users::InteractionsController < BaseController
    def index
      respond_with collection
    end

    def create
      resource.update_attributes! permitted_params
      respond_with :users, resource
    end

    private

    def collection
      current_user.interactions
    end

    def resource
      @resource ||= collection.find_or_initialize_by(activity_id: params[:activity_id])
    end

    def permitted_params
      params.require(:interaction).permit(:status, :duration)
    end
  end
end
