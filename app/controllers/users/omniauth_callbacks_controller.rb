class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def office365
    redirect_to redirect_url
  end

  def setup
    request.env['omniauth.strategy'].options[:client_id] = current_organization.office365_app_id
    request.env['omniauth.strategy'].options[:client_secret] = current_organization.office365_app_secret
    head :not_found
  end

  private

  def redirect_url
    url = request.env['omniauth.params']['redirect_url']
    "#{url}#{url.include?('?') ? '&' : '?'}auth_token=#{resource&.authentication_token}"
  end
  
  def resource
    current_organization.users.find_by(email: request.env['omniauth.auth'].info.email)
  end
end
