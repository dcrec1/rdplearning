module Users
  class NotificationsController < BaseController
    def index
      respond_with current_user.notifications
    end
  end
end
