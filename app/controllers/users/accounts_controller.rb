module Users
  class AccountsController < BaseController
    def show
      respond_with current_user
    end

    def performance
      respond_with current_user.performance
    end
  end
end
