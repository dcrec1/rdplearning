module Users
  class PublisherController < BaseController
    def token
      render json: { timestamp: timestamp, token: md5 }
    end

    private

    def md5
      Digest::MD5.hexdigest "#{params[:edition_id]}#{timestamp}#{private_key}"
    end

    def timestamp
      @timestamp ||= Time.now.to_i
    end

    def private_key
      Rails.application.secrets.publisher['private_key']
    end
  end
end
