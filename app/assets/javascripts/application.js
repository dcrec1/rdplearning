// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).on('turbolinks:load', function() {
  window.ACTIVITY = {}
  $("<a class='new-property'>+</a>").insertAfter("label[for=activity_properties]");

  $("body").on("click", ".new-property", function() {
    if ($("#activity_kind").val() == "" || $("#activity_kind").val() =="publisher") {
      $("a.publisher").addClass("active");
      $(".publisher-form").show();
      if ($("#activity_properties").val().length != 0) {
        var properties = JSON.parse($("#activity_properties").val().replace(/'/g, '"'));
        $("input[name=domain]").val(properties.domain);
        $("input[name=edition_id]").val(properties.edition_id);
        $("input[name=publication_id]").val(properties.publication_id);
      };
    } else if ($("#activity_kind").val() == "video") {
      $("a.skype").addClass("active");
      $(".videoconference-form").show()
      $("input[name=skype_url]").val($("#activity_properties").val());
    } else if ($("#activity_kind").val() == "pdf") {
      $("a.pdf").addClass("active");
      $(".pdf-form").show()
      $("input[name=pdf_url]").val($("#activity_properties").val());
    }
    $(".modal-overlay").fadeIn();
    ACTIVITY.type = "publisher";
  });

  $("body").on("click", ".overlay, .cancel", function() {
    $(".modal-overlay").fadeOut(function() {
      $(".properties-menu a").removeClass("active");
      $(".activity-type").hide();
    });
  })

  $("body").on("click", ".properties-menu a", function() {
    ACTIVITY.type = $(this).attr("data-type")
    $(".properties-menu a").removeClass("active");
    $(this).addClass("active");
    $(".activity-type").hide();
    $("."+$(this).attr("data-target")).show();
  })

  $("body").on("click", ".insert", function() {
    if (ACTIVITY.type == "publisher") {
      $("#activity_properties").val("{'domain': '"+$("input[name=domain]").val()+"', 'edition_id': "+$("input[name=edition_id]").val()+", 'publication_id': '"+$("input[name=publication_id]").val()+"'}")
      $("#activity_kind").val("publisher");
    } else if (ACTIVITY.type == "video") {
      $("#activity_properties").val($("input[name=skype_url]").val());
      $("#activity_kind").val("video");
    } else if (ACTIVITY.type == "pdf") {
      $("#activity_properties").val($("input[name=pdf_url]").val());
      $("#activity_kind").val("pdf");
    }
    $(".modal-overlay").fadeOut();
    $(".activity-type").fadeOut("slow");
  })
});
