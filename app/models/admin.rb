class Admin < ApplicationRecord
  has_and_belongs_to_many :organizations
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable
end
