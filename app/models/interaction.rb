class Interaction < ApplicationRecord
  belongs_to :user
  belongs_to :activity
  has_many :contexts, through: :activity
  after_create :validate_completion

  private

  def validate_completion
    contexts.each do |context|
      create_notification(context) if interacted_with_all_activities?(context)
    end
  end

  def create_notification(context)
    Notification.create!(user: user, body: "Parabéns! Você concluiu a(o) #{context.level.name} #{context.name}.")
  end

  def interacted_with_all_activities?(context)
    context.activities.count == context.interactions.where(user_id: user).count
  end
end
