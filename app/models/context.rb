class Context < ApplicationRecord
  belongs_to :organization
  belongs_to :level
  has_many :activities_context
  has_many :activities, through: :activities_context
  has_many :interactions, through: :activities
  has_many :memberships
  has_many :users, through: :memberships
  has_many :events
  has_many :notifications
  has_ancestry cache_depth: true

  def ancestry=(value)
    super value.blank? ? nil : value
  end

  def tree
    family.arrange_serializable[0]
  end

  def parent_enum
    self.class.where.not(id: id).pluck(:name, :id)
  end

  def serializable_hash(params = {})
    super params.merge(include: [:activities, :level])
  end

  def family
    subtree.ordered_by_ancestry.or(path)
  end
end
