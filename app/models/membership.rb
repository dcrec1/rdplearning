class Membership < ApplicationRecord
  belongs_to :user
  belongs_to :context
  enum role: %w(student teacher)
end
