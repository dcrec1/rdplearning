class User < ApplicationRecord
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :token_authenticatable, :omniauthable, omniauth_providers: [:office365]
  mount_uploader :avatar, AvatarUploader
  belongs_to :organization
  has_many :answers
  has_many :interactions
  has_many :memberships
  has_many :notifications
  has_many :contexts, through: :memberships

  def notifications
    Notification.where(user_id: self).or(Notification.where(context_id: contexts_family_ids))
  end

  def events
    Event.where(context_id: contexts_family_ids)
  end

  def performance
    answers.joins(activity: :contexts).order('contexts.ancestry_depth, contexts.position, activities_contexts.position, answers.object_id').select('answers.activity_id, contexts.ancestry_depth as context_depth, contexts.position as context_position, activities_contexts.position as activities_context_position, answers.object_id, (case when given_option = expected_option then 1 else 0 end) as score, (select avg(case when given_option = expected_option then 1 else 0 end) from answers a where a.activity_id = answers.activity_id and a.object_id = answers.object_id and a.user_id != answers.user_id) as average')
  end

  private

  def contexts_family_ids
    contexts.map { |context| context.family.pluck(:id) }.flatten
  end

  def password_required?
    false
  end
end
