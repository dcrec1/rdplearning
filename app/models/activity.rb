class Activity < ApplicationRecord
  belongs_to :organization
  has_many :activities_context
  has_many :answers
  has_many :contexts, through: :activities_context
  has_many :interactions
  enum kind: %w(publisher pdf video)
end
