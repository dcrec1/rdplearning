class Level < ApplicationRecord
  belongs_to :project
  belongs_to :version
  belongs_to :parent, class_name: 'Level'
  has_many :contexts
  validates :parent_id, uniqueness: true
end
