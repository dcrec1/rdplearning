class Notification < ApplicationRecord
  belongs_to :context
  belongs_to :user
end
