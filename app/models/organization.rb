class Organization < ApplicationRecord
  has_and_belongs_to_many :admins
  has_many :activities
  has_many :activities_contexts, through: :contexts
  has_many :answers, through: :activities
  has_many :contexts
  has_many :events, through: :contexts
  has_many :interactions, through: :activities
  has_many :memberships, through: :contexts
  has_many :notifications, through: :users
  has_many :projects
  has_many :levels, through: :projects
  has_many :users
  has_many :versions
end
