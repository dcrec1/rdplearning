class ActivitiesContext < ApplicationRecord
  belongs_to :activity
  belongs_to :context
  default_scope -> { order(:position) }
end
