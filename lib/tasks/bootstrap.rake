namespace :db do
  desc 'Bootstraps with demo data'
  task bootstrap: :environment do
    Admin.create! email: 'admin@email.com', password: 'password'
  end
end
